const express  = require('express');
const passport = require('passport');
const { User } = require('../database/schemas');

const router = express.Router();

module.exports = router;

router.post('/register', (req, res) => {
  if (!req || !req.body || !req.body.username || !req.body.password) {
    res.status(400).send({ message: 'שם משתמש וגם סיסמא נדרשים' });
  }

  req.body.username_case = req.body.username;
  req.body.username = req.body.username.toLowerCase();

  const { username } = req.body;
  const newUser = User(req.body);

  User.find({ username }, (err, users) => {
    if (err) {
      res.status(400).send({ message: 'יצירת משתמש נכשלה', err });
    }
    if (users[0]) {
      res.status(400).send({ message: 'שם המשתמש קיים במערכת' });
    }

    newUser.hashPassword().then(() => {
      newUser.save((err, savedUser) => {
        if (err || !savedUser) {
          res.status(400).send({ message: 'יצירת משתמש נכשלה', err });
        } else {
          res.send({ message: 'משתמש נוצר בהצלחה', user: savedUser.hidePassword() });
        }
      });
    });

  });
});

router.post('/login', (req, res, next) => {
  req.body.username = req.body.username.toLowerCase();

  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).send(info);
    }

    req.login(user, err => {
      if (err) {
        res.status(401).send({ message: 'התחברות נכשלה', err });
      }
      res.send({ message: 'התחברת הצליחה', user: user.hidePassword() });
    });

  })(req, res, next);
});

router.post('/logout', (req, res) => {
  req.session.destroy(err => {
    if (err) {
      res.status(400).send({ message: 'התנתקות נכשלה', err });
    }
    req.sessionID = null;
    req.logout();
    res.send({ message: 'התנתקת בהצלחה' });
  });
});
