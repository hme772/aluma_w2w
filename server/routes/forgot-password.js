const express = require("express");
const { requireAuth } = require("./middleware");
const router = express.Router();
const { User } = require("../database/schemas");
const bcrypt = require("bcryptjs");

module.exports = router;
const sgMail = require("@sendgrid/mail");
const sgMailApiKey = 
"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";
  //"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";

sgMail.setApiKey(sgMailApiKey);

sgMail.setApiKey(sgMailApiKey);
router.post("/reset", (req, res) => {
  let email = req.body.email;
  let password = Math.random().toString(16).slice(4);
  User.find({ email: email }, (err, users) => {
    if (err) {
      res.status(400).send({ message: "בדיקת מייל נכשלה", err, email });
    } else if (users && users[0]) 
    {
      console.log(password);
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) {
            res.status(400).send({ err, message: "ביקריפט לא עבד" });
          } else {
            User.findByIdAndUpdate(
              { _id: users[0]._id },{ password: hash },
              (err) => {
                if (err) {
                  res.status(400).send({ err, message: "שגיאה, סיסמא לא עודכנה" });
                } else {
                  //if succeeded changing the password
                  const msg1 = {
                    to: email,
                    from: "alumaw2w@gmail.com",
                    subject: "אלומה- איפוס סיסמא",
                    text: `שלום. <br> ברוכים הבאים לאיפוס הסיסמא במערכת אלומית. <br>   הסיסמא החדשה: ${password}  `,
                    html: `<p>שלום. <br> ברוכים הבאים לאיפוס הסיסמא במערכת אלומית. <br>     הסיסמא החדשה: <b> ${password}</b></p>`,
                  };

                  sgMail.send(msg1).then(() => {
                      //Celebrate
                      console.log("Email Sent!");
                      res
                        .status(200)
                        .send({
                          message:
                            " !אימייל נשלח! סיסמא עודכנה",
                          password: password,
                        });
                    })
                    .catch(error => {
                        // Log friendly error
                        console.error(error);
                    
                        if (error.response) {
                          // Extract error msg
                          const {message, code, response} = error;
                    
                          // Extract response msg
                          const {headers, body} = response;
                    
                          console.error(body);
                        }
                      });
                }
              }
            );
          }
        });
      });
    }
    else{
        res.status(400).send({message:"לא קיים מייל זה במערכת"});
    }
  });
});


router.post('/checkemail-reset-password', (req, res) => {
    const email = req.body.email.toLowerCase();
  const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;
  let isAluma=email.split('@')[1];
  console.log(email);
    User.find({ email: email }, (err, users) => {
       // console.log(users);

      if (err ) {
        res.status(400).send({ message: 'בדיקת מייל נכשלה', err, email });
      }
      else  if(!validRegex.test(email))
      {
        res.send({ available: false, message: 'example@aluma.org.il אימייל צריך להראות כך', email });
      } 

      /*else if(isAluma != "aluma.org.il")
      {
        res.send({ available: false, message: 'אימייל לא שייך לאלומה', email });
      }*/
      else if (users && users[0]) {
        //console.log(users[0]);
        res.send({ available: true, message: 'אימייל קיים במערכת', email });
      }   
  
      else {
        res.send({ available: false, message: 'לא ניתן לאפס סיסמא למשתמש שאינו קיים', email });
      }
    });
  });
  






/*
const msg = {
  to: "test@example.com", // Change to your recipient
  from: "test@example.com", // Change to your verified sender
  subject: "Sending with SendGrid is Fun",
  text: "and easy to do anywhere, even with Node.js",
  html: "<strong>and easy to do anywhere, even with Node.js</strong>",
};
sgMail
  .send(msg)
  .then(() => {
    console.log("Email sent2222");
  })
  .catch((error) => {
    console.error(error);
  });
module.exports.sendEmail = (email, password) => {
  console.log(email + " : " + password);
  sgMail
    .send({
      to: email,
      from: "alumaw2w@gmail.com",
      subject: "אלומה- איפוס סיסמא",
      text: `שלום. <br> ברוכים הבאים לאיפוס הסיסמא במערכת אלומית. <br>  ${password} :הסיסמא החדשה  `,
      html: `<p>שלום. <br> ברוכים הבאים לאיפוס הסיסמא במערכת אלומית. <br> <b> ${password}</b> :הסיסמא החדשה </p>`,
    })
    .then(
      () => {},
      (error) => {
        console.error(error);

        if (error.response) {
          console.error(error.response.body);
        }
      }
    );
};

module.exports.sendTemplate = (to, from, templateId, dynamic_template_data) => {
  const msg = {
    to,
    from: { name: "איפוס סיסמא", email: from },
    templateId,
    dynamic_template_data,
  };
  console.log(msg);
  sgMail
    .send(msg)
    .then((response) => {
      console.log("mail-sent-successfully", {
        templateId,
        dynamic_template_data,
      });
      console.log("response", response);
      // assume success
    })
    .catch((error) => {
      // log friendly error
      console.error("send-grid-error: ", error.toString());
    });
};
*/
