const express = require('express');
const { User } = require('../database/schemas');

const router   = express.Router();

module.exports = router;

router.get('/all', (req, res) => {
  console.log("new request all")
  User.find({}).then((users) => {
        let actives = [];
        users.forEach(user => {
                actives.push(user);
            
        });
        console.log(actives);
        res.status(200).send({
          available: true,
          message: 'ok',
            data: actives,
        });
    

});});



router.post('/checkusername', (req, res) => {
  const username = req.body.username.toLowerCase();
  console.log(username);

  User.find({ username }, (err, users) => {
    if (err) {
      res.status(400).send({ message: 'בדיקת שם משתמש נכשלה', err, username });
    }
    else if (users && users[0]) {
      res.send({ available: false, message: ' שם משתמש קיים במערכת', username });
    } else {
      res.send({ available: true, message: 'שם משתמש זמין', username });
    }
  });
});



router.post('/checkemail', (req, res) => {
  const email = req.body.email.toLowerCase();
const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;
let isAluma=email.split('@')[1];

  User.find({ email: email }, (err, users) => {
    if (err ) {
      res.status(400).send({ message: 'בדיקת מייל נכשלה', err, email });
    }
    else if (users && users[0]) {
      console.log(users[0]);
      res.send({ available: false, message: 'אימייל קיים במערכת', email });
    }   
    else  if(!validRegex.test(email))
    {
      res.send({ available: false, message: 'example@aluma.org.il אימייל צריך להראות כך', email });
    } 
    else if(isAluma != "aluma.org.il")
    {
      res.send({ available: false, message: 'אימייל לא שייך לאלומה', email });
    }
    else {
      res.send({ available: true, message: 'אימייל זמין', email });
    }
  });
});




