const LocalStrategy = require('passport-local').Strategy;
const { User } = require('../database/schemas');

const Strategies = module.exports;
function reverseString(str) {
  return str.split("").reverse().join("");
}

Strategies.local = new LocalStrategy((username, password, done) => {
  console.log(reverseString(username.trim()));
  User.findOne({ username }, (err, user) => {
    if (err) { return done(err); }
    if (!user) {
      return done(null, false, { message: 'שם משתמש אינו קיים' });
    }
    if (!user.validPassword(password)) {
      return done(null, false, { message: 'סיסמא שגויה או שם משתמש שגוי' });
    }
    return done(null, user);
  });
});
