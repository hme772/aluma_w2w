import { push } from 'connected-react-router';
import { snakeToCamelCase } from 'json-style-converter/es5';
import { store as RNC } from 'react-notifications-component';

import { postResetPassword } from '_api/reset';

import { dispatchError } from '_utils/api';


export const attemptReset = email => dispatch =>
postResetPassword(email)
    .then(data => {
      RNC.addNotification({
        title: '!הצלחה',
        message: data.message,
        type: 'success',
        container: 'top-right',
        animationIn: ['animated', 'fadeInRight'],
        animationOut: ['animated', 'fadeOutRight'],
        dismiss: {
        duration: 5000,
        },
      });

      return data;
    })
    .then(() => dispatch(push('/login')))
    .catch(dispatchError(dispatch));




