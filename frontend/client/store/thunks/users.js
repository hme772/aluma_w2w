import { snakeToCamelCase } from 'json-style-converter/es5';
import R from 'ramda';

import { getUsers } from '_api/users';
import { setTodos, addTodo, toggleCompleteTodo, updateTodo, removeTodo } from '_actions/todos';
import {setUsers} from '_actions/users'

import { dispatchError } from '_utils/api';

export const attemptGetUsers = () => dispatch =>
  getUsers()
    .then(data => {
      const users = R.map(user =>
        R.omit(['username_case'], R.assoc('username_case', user.username_case, snakeToCamelCase(user))), data.users);

      dispatch(setUsers(users));
      return data.users;
    })
    .catch(dispatchError(dispatch));

    

export const attemptAddTodo = text => dispatch =>
  postTodo({ text })
    .then(data => {
      const todo = R.omit(['Id'], R.assoc('id', data.todo._id, snakeToCamelCase(data.todo)));

      dispatch(addTodo(todo));
      return data.user;
    })
    .catch(dispatchError(dispatch));

export const attemptToggleCompleteTodo = id => dispatch =>
  putToggleCompleteTodo({ id })
    .then(data => {
      dispatch(toggleCompleteTodo(id));
      return data;
    })
    .catch(dispatchError(dispatch));

export const attemptUpdateTodo = (id, text) => dispatch =>
  putTodo({ id, text })
    .then(data => {
      dispatch(updateTodo({ id, text, updatedAt: data.todo.updated_at }));
      return data;
    })
    .catch(dispatchError(dispatch));

export const attemptDeleteTodo = id => dispatch =>
  deleteTodo({ id })
    .then(data => {
      dispatch(removeTodo(id));
      return data;
    })
    .catch(dispatchError(dispatch));
