import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import R from 'ramda';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons/faExclamationTriangle';

import Box from 'react-bulma-companion/lib/Box';
import Button from 'react-bulma-companion/lib/Button';
import Title from 'react-bulma-companion/lib/Title';
import Field from 'react-bulma-companion/lib/Field';
import Control from 'react-bulma-companion/lib/Control';
import Icon from 'react-bulma-companion/lib/Icon';
import Input from 'react-bulma-companion/lib/Input';
import Label from 'react-bulma-companion/lib/Label';
import Help from 'react-bulma-companion/lib/Help';

import useKeyPress from '_hooks/useKeyPress';
import { postCheckUsername ,postCheckEmail } from '_api/users';
import { validateUsername, validatePassword , validateEmail } from '_utils/validation';
import { attemptRegister } from '_thunks/auth';
import './Register.module.css';
import Logo from "../../../assets/images/brand-logo.jpg";



export default function Register() {
  const dispatch = useDispatch();

  const [username, setUsername] = useState('');
  const [usernameMessage, setUsernameMessage] = useState('');

  const [email, setEmail] = useState('');
  const [emailMessage, setEmailMessage] = useState('');

  const [password, setPassword] = useState('');
  const [passwordMessage, setPasswordMessage] = useState('');

  const [usernameAvailable, setUsernameAvailable] = useState(false);
  const [emailAvailable, setEmailAvailable] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);

  const checkPassword = (newUsername, newPassword) => {
    const { valid, message } = validatePassword(newUsername, newPassword);

    setPasswordValid(valid);
    setPasswordMessage(message);
  };

  const checkUsername = newUsername => {
    const { valid, message } = validateUsername(newUsername);

    if (valid) {
      setUsernameMessage('Checking username...');
      setUsernameAvailable(false);
     
     postCheckUsername(newUsername) .then(res => 
          {
          
          setUsernameAvailable(res.available);
          setUsernameMessage(res.message);
        }
        )
        .catch(R.identity);
    } else {
      setUsernameAvailable(valid);
      setUsernameMessage(message);
    }
  };


  const checkEmail = newEmail => {
    const { valid, message } = validateEmail(newEmail);

    if (valid) {
      setEmailMessage('Checking email...');
      setEmailAvailable(false);
      
      postCheckEmail(newEmail)
        .then(res => {
          setEmailAvailable(res.available);
          setEmailMessage(res.message);
        }) .catch(R.identity);
    } else {
      setEmailAvailable(valid);
      setEmailAvailable(message);
    }
  };

  const updateUsername = newUserName => {
    setUsername(newUserName);
    checkEmail(email);
    checkPassword(newUserName, password);
  };
  const updateEmail = newEmail =>
  {
    setEmail(newEmail.toLowerCase());
  }

  const handleUsernameChange = e => {
    updateUsername(e.target.value);
    checkUsername(e.target.value);
  };

  const handleEmailChange = e => {
    updateEmail(e.target.value);
    checkEmail(e.target.value);
  };

  const handlePasswordChange = e => {
    setPassword(e.target.value);
    checkPassword(username, e.target.value);
  };

  const register = () => {
    if (usernameAvailable && passwordValid && emailAvailable) {
      const newUser = {
        username,
        email,
        password,
      };

      dispatch(attemptRegister(newUser))
        .catch(R.identity);
    }
  };

  useKeyPress('Enter', register);

  return (
    <Box className="box">
       <img className='Logo' src={Logo} alt='logo'/>
       <br/><br/>
       <br/>
      <div className="Alumit-14"></div>
      <Title size="3">
      הרשמה למערכת אלומית
      </Title>
      <hr className="separator" />
      <p className="has-space-below">
       &nbsp;
        <Link to="/login">
          כניסה לחשבון קיים
        </Link>
      </p>
      <Field>
        <Label className='label' htmlFor="username">
          שם משתמש
        </Label>
        <Control iconsRight>
          <Input
            id="username"
            placeholder="Username"
            color={username ? (usernameAvailable ? 'success' : 'danger') : undefined}
            value={username}
            onChange={handleUsernameChange}
          />
          {username && (
            <Icon
              size="small"
              align="right"
              color={usernameAvailable ? 'success' : 'danger'}
            >
              <FontAwesomeIcon
                icon={usernameAvailable ? faCheck : faExclamationTriangle}
              />
            </Icon>
          )}
        </Control>
        {username && (
          <Help color={usernameAvailable ? 'success' : 'danger'}>
            {usernameMessage}
          </Help>
        )}
      </Field>
     
      <Field>
        <Label className='label' htmlFor="email">
        אימייל עבודה
        </Label>
        <Control iconsRight>
          <Input
            id="email"
            placeholder="email"
            color={email ? (emailAvailable ? 'success' : 'danger') : undefined}
            value={email}
            onChange={handleEmailChange}
          />
          {email && (
            <Icon
              size="small"
              align="right"
              color={emailAvailable ? 'success' : 'danger'}
            >
              <FontAwesomeIcon
                icon={emailAvailable ? faCheck : faExclamationTriangle}
              />
            </Icon>
          )}
        </Control>
        {email && (
          <Help color={emailAvailable ? 'success' : 'danger'}>
            {emailMessage}
          </Help>
        )}
      </Field>



      <Field>
        <Label htmlFor="password">
          סיסמא
        </Label>
        <Control iconsRight>
          <Input
            id="password"
            placeholder="Password"
            type="password"
            color={password ? (passwordValid ? 'success' : 'danger') : undefined}
            value={password}
            onChange={handlePasswordChange}
          />
          {password && (
            <Icon
              size="small"
              align="right"
              color={passwordValid ? 'success' : 'danger'}
            >
              <FontAwesomeIcon
                icon={passwordValid ? faCheck : faExclamationTriangle}
              />
            </Icon>
          )}
        </Control>
        {password && (
          <Help color={passwordValid ? 'success' : 'danger'}>
            {passwordMessage}
          </Help>
        )}
      </Field>
      <br/>
      <hr className="separator" />
      <div className="has-text-right">
        <Button color="success" onClick={register} disabled={!passwordValid || !usernameAvailable || !emailAvailable}>
          יצירת משתמש
        </Button>
      </div>
    </Box>
  );
}
//disabled={!passwordValid || !usernameAvailable || !emailAvailable}