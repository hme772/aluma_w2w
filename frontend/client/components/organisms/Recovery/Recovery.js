import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import R from 'ramda';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons/faExclamationTriangle';

import Box from 'react-bulma-companion/lib/Box';
import Button from 'react-bulma-companion/lib/Button';
import Title from 'react-bulma-companion/lib/Title';
import Field from 'react-bulma-companion/lib/Field';
import Control from 'react-bulma-companion/lib/Control';
import Icon from 'react-bulma-companion/lib/Icon';
import Input from 'react-bulma-companion/lib/Input';
import Label from 'react-bulma-companion/lib/Label';
import Help from 'react-bulma-companion/lib/Help';

import useKeyPress from '_hooks/useKeyPress';
import { postCheckEmailReset } from '_api/reset';
import {  validateEmail } from '_utils/validation';
import { attemptReset } from '_thunks/reset';
import './Recovery.module.css';
import Logo from "../../../assets/images/brand-logo.jpg";



export default function Recovery() {
  const dispatch = useDispatch();



  const [email, setEmail] = useState('');
  const [emailMessage, setEmailMessage] = useState('');

  const [emailAvailable, setEmailAvailable] = useState(false);

  const checkEmail = newEmail => {
    const { valid, message } = validateEmail(newEmail);

    if (valid) {
      setEmailMessage('Checking email...');
      setEmailAvailable(false);
      
      postCheckEmailReset(newEmail)
        .then(res => {
          setEmailAvailable(res.available);
          setEmailMessage(res.message);
        }) .catch(R.identity);
    } else {
      setEmailAvailable(valid);
      setEmailAvailable(message);
    }
  };

 
  const updateEmail = newEmail =>
  {
    setEmail(newEmail.toLowerCase());
  }


  const handleEmailChange = e => {
    updateEmail(e.target.value);
    checkEmail(e.target.value);
  };

  /*useEffect(()=>{
    const identifier =  setTimeout(()=>{ 
      reset();
       },500);
    return ()=>{
  clearTimeout(identifier);}}
  ,[email,emailAvailable]);*/
  function reset() {
    if (emailAvailable) {
       dispatch(attemptReset(email)).then(res=>{alert("נא להכנס למייל ולהשתמש בסיסמא ששם")})
        .catch(R.identity);
        dispatch(push('/login'));
    }}
  useKeyPress('Enter', reset);

  return (
    <Box className="box">
       <img className='Logo' src={Logo} alt='logo'/>
       <br/><br/>
       <br/>
      <div className="Alumit-14"></div>
      <Title size="3">
      איפוס סיסמא
      </Title>
      <hr className="separator" />
      <p className="has-space-below">
       &nbsp;
        <Link to="/login">
          כניסה לחשבון קיים
        </Link>
      </p>
      
      <Field>
        <Label className='label' htmlFor="email">
        אימייל עבודה
        </Label>
        <Control iconsRight>
          <Input
            id="email"
            placeholder="email"
            color={email ? (emailAvailable ? 'success' : 'danger') : undefined}
            value={email}
            onChange={handleEmailChange}
          />
          {email && (
            <Icon
              size="small"
              align="right"
              color={emailAvailable ? 'success' : 'danger'}
            >
              <FontAwesomeIcon
                icon={emailAvailable ? faCheck : faExclamationTriangle}
              />
            </Icon>
          )}
        </Control>
        {email && (
          <Help color={emailAvailable ? 'success' : 'danger'}>
            {emailMessage}
          </Help>
        )}
      </Field>
      
      <br/>
      <hr className="separator" />
      <div className="has-text-right">
        <Button color="success" onClick={reset} disabled={ !emailAvailable}>
          איפוס 
        </Button>
      </div>
    </Box>
  );
}
//disabled={!passwordValid || !usernameAvailable || !emailAvailable}