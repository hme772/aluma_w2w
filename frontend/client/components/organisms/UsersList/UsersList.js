import React,{useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import R from 'ramda';
import User from '_molecules/User';

export default function UsersList() 
{
  const [users, setUsers] = useState([]);
  useEffect(() => {
    async function getUsers() {
      const response = await fetch(`http://localhost:3000/api/users/all`);
  
      if (response.status != 200) {
        const message = `An error occurred: ${response.statusText}`;
        window.alert(message);
        return;
      }
  
      const users = await response.json();
      //users= users;
      setUsers(users.data);
    }
  
    getUsers();
  
    return;
  }, [users.length]);





  /*async function getAll()
  {
      let allusers=[];
      let response = await fetch("/users/all", {
          method: 'get',
      });
      let users = await response.json();
      users.data.forEach(function (user) {
          allusers.push(user);
      });
      return allusers;
  }
*/

  //const  users  = useSelector(R.pick(['users']));

 //console.log(getAll());

   

  
  return (
     
      <ul className="todo-list">
        {R.reverse(users).map(user => <User name={user.username} {...user} />)}
      </ul>
  
  );
}

