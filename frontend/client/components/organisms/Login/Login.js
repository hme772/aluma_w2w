import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import R from 'ramda';

import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';
import { faLock } from '@fortawesome/free-solid-svg-icons/faLock';

import Box from 'react-bulma-companion/lib/Box';
import Block from 'react-bulma-companion/lib/Block';
import Title from 'react-bulma-companion/lib/Title';
import Control from 'react-bulma-companion/lib/Control';
import Button from 'react-bulma-companion/lib/Button';
import Checkbox from 'react-bulma-companion/lib/Checkbox';

import useKeyPress from '_hooks/useKeyPress';
import { attemptLogin } from '_thunks/auth';
import FormInput from '_molecules/FormInput';

import './Login.module.css';
import Logo from "../../../assets/images/brand-logo.jpg";




export default function Login() {//stam
  const dispatch = useDispatch();

  const [remember, setRemember] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    const username = localStorage.getItem('username');
    if (username) {
      setRemember(true);
      setUsername(username);
    }
  }, []);

  const login = () => {
    const userCredentials = { username, password };

    if (remember) {
      localStorage.setItem('username', username);
    }

    dispatch(attemptLogin(userCredentials))
      .catch(R.identity);
  };

  useKeyPress('Enter', login);

  const rememberMe = () => {
    localStorage.removeItem('username');
    setRemember(!remember);
  };

  const updateUsername = e => setUsername(e.target.value);
  const updatePassword = e => setPassword(e.target.value);

  return (
    <Box className="login">
        <img className='Logo' src={Logo} alt='logo'/>
       <br/><br/>
       <br/>
      <Title size="3">
        כניסה למערכת אלומית
      </Title>
      <hr className="separator" />
      <Block>
        &nbsp;
        <Link to="/register">
          יצירת חשבון חדש
        </Link>
      </Block>
      <FormInput
        onChange={updateUsername}
        placeholder="Username"
        value={username}
        rightIcon={faUser}
      />
      <FormInput
        onChange={updatePassword}
        placeholder="Password"
        value={password}
        rightIcon={faLock}
        type="password"
      />
      <Block>
        <Link to="/recovery">
          שכחתי סיסמא
        </Link>
      </Block>
      <hr className="separator" />
      <Control className="is-clearfix">
      <Checkbox>
      <span className='span'>&nbsp; זכור אותי   </span>
          <input id="check" className="checkbox" type="checkbox" onChange={rememberMe} checked={remember} />
        </Checkbox>
        <br/>
        <br/>
        <Button className="is-pulled-right" color="success" onClick={login}>
          כניסה
        </Button>
        
        
      </Control>
    </Box>
  );
}
