import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import R from 'ramda';

import Recovery from '_templates/RecoverySection';

export default function RecoveryPage() {
  const dispatch = useDispatch();
  const { user } = useSelector(R.pick(['user']));

  useEffect(() => {
    if (!R.isEmpty(user)) {
      dispatch(push('/login'));
    }
  }, []);

  return (
    <div className="recovery-page page-recovery">
      <Recovery />
    </div>
  );
}


