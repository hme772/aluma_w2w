import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import R from 'ramda';

import { attemptGetUsers } from '_thunks/users';

import UsersSection from '_templates/UsersSection';
import User from '_molecules/User';
import UserList from '_organisms/UsersList';
export default function UsersPage() {

  const { user } = useSelector(R.pick(['user']));
  console.log(user.username);

  console.log("once again");
  return (
    <div >
      <UsersSection/>
    </div>
  );
}
