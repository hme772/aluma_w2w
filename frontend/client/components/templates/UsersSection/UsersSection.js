import React from 'react';
import UsersList from '_organisms/UsersList';

import Section from 'react-bulma-companion/lib/Section';
import Title from 'react-bulma-companion/lib/Title';
import Columns from 'react-bulma-companion/lib/Columns';
import Column from 'react-bulma-companion/lib/Column';

export default function UsersSection() {
  console.log("here i am");
  return (
    <Section className="todos-section">
      <Title size="1" className="has-text-centered">
        Users List:
      </Title>
      <Columns>
        <Column size="8" offset="2" className="has-text-centered">
        </Column>
      </Columns>
      <Columns>
        <Column size="8" offset="2" className="has-text-left">
          <UsersList />
        </Column>
      </Columns>
    </Section>
  );
}
