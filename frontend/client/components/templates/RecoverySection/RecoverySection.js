import React from 'react';
import Recovery from '_organisms/Recovery';

import Section from 'react-bulma-companion/lib/Section';

export default function RecoverySection() {
  return (
    <Section className="recovery-secction">
      <Recovery />
    </Section>
  );
}
