import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { parseISO, formatDistanceToNow } from 'date-fns';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';



export default function User(props) {
  
  return (
   <li>{props.name}</li>
  );
}

/*
 <li className="user box">
      <Box className="general-profile">
      <Icon size="medium" className="is-pulled-right" onClick={refresh} onKeyPress={refresh}>
        <FontAwesomeIcon icon={faSync} size="lg" />
      </Icon>
      <Title size="3">
        General
      </Title>
      <hr className="separator" />
      <Columns>
        <Column size="4">
          <Title size="3" className="has-text-centered">
            {user.usernameCase}
          </Title>
          <Image>
            <Image.Content
              className="profile-img"
              src={user.profilePic }
              alt="Profile"
            />
          </Image>
          <Field>
            <Label htmlFor="profile-pic-url">
              Picture URL
            </Label>
            <Control>
              <Input
                id="profile-pic-url"
                placeholder="Picture URL"
                value={profilePic}
                onChange={updateProfilePic}
              />
            </Control>
          </Field>
        </Column>
        <Column size="8">
          <Columns>
            <Column size="6">
              <Field>
                <Label htmlFor="first-name" className="Label">
                  First Name
                </Label>
                <Control>
                  <Input
                    id="first-name"
                    placeholder="First Name"
                    value={firstName}
                    onChange={updateFirstName}
                  />
                </Control>
              </Field>
            </Column>
            <Column size="6">
              <Field>
                <Label htmlFor="last-name">
                  Last Name
                </Label>
                <Control>
                  <Input
                    id="last-name"
                    placeholder="Last Name"
                    value={lastName}
                    onChange={updateLastName}
                  />
                </Control>
              </Field>
            </Column>
          </Columns>
          <Field>
            <Label htmlFor="bio">
              Bio
            </Label>
            <Control>
              <Textarea
                id="bio"
                placeholder="Tell us about yourself."
                value={bio}
                maxLength={240}
                onChange={updateBio}
              />
            </Control>
            <Help>
              {`Characters remaining: ${charactersRemaining}`}
            </Help>
          </Field>
        </Column>
      </Columns>
      <hr className="separator" />
      <Button color="success" onClick={save} disabled={!edited}>
        Save
      </Button>
    </Box>
  
    </li>

*/