import request from 'superagent';
import { handleSuccess, handleError } from '_utils/api';

const apiURL = process.env.API_URL || '';

export const postCheckUsername = username =>
  request.post(`http://localhost:3000/api/users/checkusername`)
    .send({ username })
    .then(handleSuccess)
    .catch(handleError);
export const postCheckEmail = email =>
  request.post(`${apiURL}/api/users/checkemail`)
    .send({ email })
    .then(handleSuccess)
    .catch(handleError);

export const getUsers = () =>
     request.get(`${apiURL}/api/users/all`)
      .withCredentials()
      .then(handleSuccess)
      .catch(handleError);