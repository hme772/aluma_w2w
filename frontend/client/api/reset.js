import request from 'superagent';
import { handleSuccess, handleError } from '_utils/api';

const apiURL = process.env.API_URL || '';

export const postResetPassword = email =>
  request.post(`http://localhost:3000/api/forgot-password/reset`)
    .send({ email })
    .then(handleSuccess)
    .catch(handleError);

    

export const postCheckEmailReset = email =>
  request.post(`${apiURL}/api/forgot-password/checkemail-reset-password`)
    .send({ email })
    .then(handleSuccess)
    .catch(handleError);