import R from 'ramda';

export const validateUsername = username => {
  let valid = true;
  let message = 'שם משתמש תקין';

  if (!R.match(/^[א-תa-zA-Z0-9_]+$/, username).length) {
    message = 'נעשה שימוש בתו לא תקין';
    valid = false;
  } else if (username.length < 4) {
    message = 'שם משתמש חייב להיות לפחות 4 תווים';
    valid = false;
  } else if (username.length > 20) {
    message = 'שם משתמש חייב להיות 20 תווים או פחות';
    valid = false;
  } else if (R.match(/[א-תa-zA-Z]/g, username).length < 4) {
    message = 'שם משתמש חייב להיות בעל 4 אותיות לפחות ';
    valid = false;
  }
  return { valid, message };
};
export const validateEmail = email =>
{
  let valid = true;
  let message = 'אימייל תקין';
  //todo: ending aluma.somthing
  return {valid, message};

}

export const validatePassword = (username, password) => {
  let valid = true;
  let message = 'סיסמא תקינה';

  if (password.length < 6) {
    valid = false;
    message = 'סיסמא חייבת לכלול לפחות 6 תווים';
  } else if (password.length > 16) {
    valid = false;
    message = 'סיסמא חייבת להיות 16 תווים או פחות';
  } else if (username === password) {
    valid = false;
    message = 'שם משתמש וסיסמא זהים';
  } else if (!R.match(/[0-9]/, password).length) {
    valid = false;
    message = 'סיסמא חייבת לכלול לפחות מספר אחד';
  }

  return { valid, message };
};

export const validateName = name => {
  if (name === '') {
    return true;
  }
  if (!R.match(/^[א-תa-zA-ZÀ-ÿ'.\s]+$/, name).length) {
    return false;
  }
  if (name.length > 20) {
    return false;
  }
  return true;
};
